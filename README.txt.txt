In order to run the application. You might have to include
JAX-WS libraries to your Tomcat library folder. 
(But if you are testing this on the lab computer they might
already have Metro libraries installed)

Please copy everything inside ADD_TO_TOMCAT folder and include them
inside Tomcat lib folder (i.e. in my case it is C:\Tomcat\apache-tomcat-6.0.35\apache-tomcat-6.0.35).
