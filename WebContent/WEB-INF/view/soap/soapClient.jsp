<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@page
	import="com.myref.mvc.helper.ReferenceListHelper,com.myref.mvc.harvard.utility.*,
	java.util.List,
	com.myref.mvc.model.Reference,
	com.myref.mvc.model.dao.*, 
	com.myref.soap.client.*, 
	java.io.InputStream, 
	java.util.Properties"%>

<%
    InputStream stream = application.getResourceAsStream("resources/properties/ServerInfo.properties");
    Properties props = new Properties();
    props.load(stream);
    String ADDRESS = props.getProperty("address");
    
%>
<html>
<head>
<base href="<%=ADDRESS%>">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Riferencio</title>
<link rel="stylesheet" href="<%=ADDRESS%>resources/css/style.css" type="text/css" />
<link rel="stylesheet" href="<%=ADDRESS%>resources/css/styleForSoapClient.css" type="text/css" />
<script type="text/javascript" src="resources/js/soapFunc/AjaxReader.js"></script>
<script type="text/javascript"
	src="resources/js/soapFunc/SoapClientController.js"></script>
<script>
	var referenceController;
	var referencesController;
	var refUrl = 'http://localhost:8080/MyReferences/resources/xml/referenceExample.xml';
	var refsUrl = 'http://localhost:8080/MyReferences/resources/xml/referencesExample.xml';
	function init(){
		var textArea = document.getElementById('MyTextArea');
		textArea.value = "Please enter in here..";
		//initialise controller for refrence
		referenceController = new SoapClientController(textArea);
		referenceController.init(refUrl);
		
		//Initialise controller for references
		referencesController = new SoapClientController(textArea);
		referencesController.init(refsUrl);

	}
</script>

</head>

<body onload="init()">
	<div id="wrapper">
		<div id="rightcontainer">
			<div id="header">
				<div id="logo">
					<a href="<%=ADDRESS%>"> <img
						src="<%=ADDRESS%>resources/images/logo.png" /> </a>
				</div>
				<!-- end of logo -->
			</div>
			<!-- end of header  -->

			<div class="clear"></div>
			<!-- clears elements -->

			<div id="leftCol">
				<div id="typeMenu">
					<ul>
						<li class="book"><a href="book">Book</a></li>
						<li class="journal"><a href="journal">Journal</a></li>
						<li class="other"><a href="client">SOAP</a></li>
					</ul>
				</div>
				<!-- end of typeMenu -->
			</div>
			<!-- end of leftCol -->
			<div id="rightCol">

				<div id="referenceList">
					<h1 class="alt">References</h1>

					<!-- Print References -->
					<table>
						<%
String error = null;
SoapServerImplService service = new SoapServerImplService();
SoapServerImpl port = service.getSoapServerImplPort();
String textAreaInput = request.getParameter("xmlBox");
if(textAreaInput != null && "POST".equalsIgnoreCase(request.getMethod())){
	if(textAreaInput.endsWith("</references>")){
		error = port.addAll(textAreaInput);
	} else if (textAreaInput.matches("^[0-9]")){
		//Delete all statement should start from a number
		error = port.deleteAll(textAreaInput);
	} else {
		error = port.add(textAreaInput);
	}
} else if (request.getParameter("refId") != null){
	error = port.delete(Integer.parseInt(request.getParameter("refId")));
}
String status = (String) request.getAttribute("status");
%>


						<%
		List<Reference> references = (List)port.listAll();
		if (references.isEmpty())
			out.println("<div class='systemMessage'>There are no references to display.</div>");
		else
			try {
				for (Reference reference : new ReferenceListHelper().references()) {
		%>
						<tr>
							<td><a
								href="<%=reference.getType() + "/view?refId=" + reference.getId()%>">
									<%=HavardReference.convert(reference, reference.getType())%> </a>
							</td>
							<td class="delete"><a
								href="<%=ADDRESS + "client?refId=" + reference.getId()%>"> <img	src="<%=ADDRESS%>resources/images/delete.png" /> </a>
							</td>
						</tr>
						<%
			}
			} catch (Exception e) {
				out.println("<div class='systemMessage'>Oops Something went wrong and the list could not be loaded!</div>");
			}
	%>
					</table>
				</div>

			</div>

			<!-- end of rightCol -->
			<div id="middleCol">
				<div id="content">
					<h1>
						<i>SOAP client</i>
					</h1>
					<!-- Displays any error from SOAP server -->
					<%if(error != null){ %>
					<div class="msg error">
						<!-- <%=error %> -->
					</div>
					<%} else {%>
					<b>Please write Reference XML into the text area underneath</b>
					<% } %>
					<form method="POST" action="client">
						<textarea id="MyTextArea" name="xmlBox" rows="10" cols="10"></textarea>
						<input class="button" type="submit" value="Add new" /> 
						<input class="button" type="button" value="Reset refrence" onclick="referenceController.resetTextArea();" />
						<input class="button" type="button" value="Reset references" onclick="referencesController.resetTextArea();" />
					</form>

				</div>
				<!-- end of content -->
			</div>
			<!-- end of middleCol -->

		</div>
		<!-- end of rightcontainer -->

		<div class="clear"></div>
		<!-- clears elements -->
		<div id="footer">
			<div id="copyright">Copyright 2012 ETJ 2012</div>
			<!-- end of copyright -->
		</div>
		<!-- end of footer -->

	</div>
	<!-- end of wrapper -->
</body>
</html>