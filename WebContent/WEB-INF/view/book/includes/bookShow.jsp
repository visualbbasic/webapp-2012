
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<table class="view">
	<tr>
		<td><h3>Author(s):</h3></td>
		<td><c:forEach var="author" items="${reference.authors}">
        		${author.surname}, ${author.initial}<br />
			</c:forEach>
		</td>
	</tr>
	<tr>
		<td><h3>Year:</h3></td>
		<td>${reference.year}</td>
	</tr>
	<tr>
		<td><h3>Title:</h3></td>
		<td>${reference.title}</td>
	</tr>
	<tr>
		<td><h3>Edition:</h3></td>
		<td>${reference.edition}</td>
	</tr>
	<tr>
		<td><h3>Publisher:</h3></td>
		<td>${reference.publisher}</td>
	</tr>
	<tr>
		<td><h3>Place of publication:</h3></td>
		<td>${reference.place}</td>
	</tr>
	<tr>
		<td><h3>URL:</h3></td>
		<td><a href="${reference.url}" target="blank">${reference.url}</a>
		</td>
	</tr>
	<tr>
		<td><h3>Comment:</h3></td>
		<td>${reference.comment}</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><h1>
				<a href="book/edit?refId=${reference.id}"> <img border="0"
					src="resources/images/edit.png" /> </a><a
					href="book/delete?refId=${reference.id}"> <img border="0"
					src="resources/images/delete.png" /> </a>
			</h1></td>
	</tr>
</table>