<tr>
	<td><form:label path="year">Year</form:label>
	</td>
	<td><form:input path="year"
			onkeypress="return numericTyper(event)" type="number" min="1"
			max="2012" />
	</td>
	<td class="systemMessage"><form:errors path="year" />
	</td>
</tr>
<tr>
	<td><form:label path="title">Book title</form:label>
	</td>
	<td><form:input type="text" path="title" />
	</td>
	<td class="systemMessage"><form:errors path="title" />
	</td>
</tr>
<tr>
	<td><form:label path="edition">Edition</form:label>
	</td>
	<td><form:input type="text" path="edition" />
	</td>
</tr>
<tr>
	<td><form:label path="publisher">Publisher</form:label>
	</td>
	<td><form:input type="text" path="publisher" />
	</td>
</tr>
<tr>
	<td><form:label path="place">Place of publication</form:label>
	</td>
	<td><form:input type="text" path="place" />
	</td>
</tr>
<tr>
	<td><form:label path="url">URL</form:label>
	</td>
	<td><form:input path="url" type="url" name="url" />
	</td>
</tr>
<tr>
	<td><form:label path="comment">Comment</form:label>
	</td>
	<td><form:input type="text" path="comment" />
	</td>
</tr>
