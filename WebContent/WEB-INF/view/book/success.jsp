<%@include file="../includes/header.jsp"%>
<h1 class="bookHeader">View Book</h1>
<div class="msg success">
	<h2>Success!</h2>
	<p>The reference list has been updated.</p>
</div>
<%@include file="includes/bookShow.jsp"%>
<%@include file="../includes/footer.jsp"%>