<%@include file="../includes/header.jsp"%>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script type="text/javascript" src="resources/js/AddRow.js"></script>
<script type="text/javascript" src="resources/js/FormValidator.js"></script>



<form:form method="POST" action="book/add" modelAttribute="reference">
	<h1 class="bookHeader">Add Book</h1>
	<form:errors path="*">
		<div class="msg error">
			<h2>Book creation failure!</h2>
			<p>
				Please make the following correction(s) before proceeding. <br /> <br />
				<form:errors path="*" class="systemMessage" />
			</p>
		</div>
	</form:errors>
	<form:input path="id" type="hidden" />
	<form:input type="hidden" path="type" value="book" />

	<table>
		<%@include file="../includes/referenceHead.jsp"%>
		<tbody>
			<%@include file="includes/bookInputs.jsp"%>
			<tr>
				<td colspan="2"><input id="bookSubmit" type="submit"
					value="Add Book" />
				</td>
			</tr>
		</tbody>
	</table>
</form:form>

<%@include file="../includes/footer.jsp"%>