<%@ page language="java"
	contentType="application/pdf; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%><%@ page
	import="java.util.*, com.myref.mvc.helper.*, java.io.*,java.net.*,
	au.edu.uts.it.wsd.*,com.myref.transformer.*"%><%
	
	//Jsp page that invokes the pdf conversion method
	
	ReferenceListHelper rLH = new ReferenceListHelper();
	String referencesXML = rLH.referencesXml();
	
	PdfFOPCreator fopc = new PdfFOPCreator();
			
	String refName = "My_References.pdf";
		
	InputStream xsltFile = application.getResourceAsStream("WEB-INF/xslTransformer/transformPDF.xslt");

	fopc.create(referencesXML, xsltFile, refName, response);

%>