
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<table class="view">
	<tr>
		<td><h3>Author(s):</h3></td>
		<td><c:forEach var="author" items="${reference.authors}">
        		${author.surname}, ${author.initial}<br />
			</c:forEach>
		</td>
	</tr>
	<tr>
		<td><h3>Year of publication:</h3></td>
		<td>${reference.year}</td>
	</tr>
	<tr>
		<td><h3>Title of the article:</h3></td>
		<td>${reference.title}</td>
	</tr>
	<tr>
		<td><h3>Journal name:</h3></td>
		<td>${reference.journal}</td>
	</tr>
	<tr>
		<td><h3>Volume number:</h3></td>
		<td>${reference.volume}</td>
	</tr>
	<tr>
		<td><h3>Issue number:</h3></td>
		<td>${reference.issue}</td>
	</tr>
	<tr>
		<td><h3>Start Page number(s):</h3></td>
		<td>${reference.startPage}</td>
	</tr>
	<tr>
		<td><h3>End Page number(s):</h3></td>
		<td>${reference.endPage}</td>
	</tr>
	<tr>
		<td><h3>URL:</h3></td>
		<td><a href="${reference.url}" target="blank">${reference.url}</a>
		</td>
	</tr>
	<tr>
		<td><h3>Comment:</h3></td>
		<td>${reference.comment}</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><h1>
				<a href="journal/edit?refId=${reference.id}"> <img border="0"
					src="resources/images/edit.png" /> </a><a
					href="journal/delete?refId=${reference.id}"> <img border="0"
					src="resources/images/delete.png" /> </a>
			</h1></td>
	</tr>
</table>