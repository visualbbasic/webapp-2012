<tr>
	<td><form:label path="year">Year of publication</form:label>
	</td>
	<td><form:input path="year"
			onkeypress="return numericTyper(event)" type="number" min="1"
			max="2012" />
	</td>
	<td class="systemMessage"><form:errors path="year" />
	</td>
</tr>
<tr>
	<td><form:label path="title">Title of the article</form:label>
	</td>
	<td><form:input type="text" path="title" />
	</td>
	<td class="systemMessage"><form:errors path="title" />
	</td>
</tr>
<tr>
	<td><form:label path="journal">Journal name</form:label>
	</td>
	<td><form:input type="text" path="journal" />
	</td>
</tr>
<tr>
	<td><form:label path="volume">Volume number</form:label>
	</td>
	<td><form:input type="text" path="volume" />
<tr>
	<td><form:label path="issue">Issue number</form:label>
	</td>
	<td><form:input type="text" path="issue" />
</tr>
<tr>
	<td><form:label path="startPage">Start Page number(s)</form:label>
	</td>
	<td><form:input type="number" path="startPage"
			onkeypress="return numericTyper(event)" />
	</td>
	<td class="systemMessage"><form:errors path="startPage" />
	</td>
</tr>
<tr>
	<td><form:label path="endPage">End Page number(s)</form:label>
	</td>
	<td><form:input type="number" path="endPage"
			onkeypress="return numericTyper(event)" />
	</td>
	<td class="systemMessage"><form:errors path="endPage" />
	</td>
</tr>
<tr>
	<td><form:label path="url">URL</form:label>
	</td>
	<td><form:input path="url" type="url" name="url" />
	</td>
</tr>
<tr>
	<td><form:label path="comment">Comment</form:label>
	</td>
	<td><form:input type="text" path="comment" />
	</td>
</tr>