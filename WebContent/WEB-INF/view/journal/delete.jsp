<%@include file="../includes/header.jsp"%>
<h1 class="bookHeader">Deleted Book</h1>

<div class="msg success">
	<h2>The following journal was deleted</h2>
</div>
<p class="systemMessage">
	<span class="alt">${reference}</span> <br /> <br /> With ID: <br />
	<span class="alt">${reference.id}</span>
</p>


<%@include file="../includes/footer.jsp"%>