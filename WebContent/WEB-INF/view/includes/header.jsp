<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@page
	import="com.myref.mvc.helper.ReferenceListHelper,
				com.myref.mvc.harvard.utility.*, 
				java.util.List, 
				com.myref.mvc.model.*,
				com.myref.mvc.model.dao.*,
				com.myref.transformer.*,
				java.util.*,
				java.io.*,
				java.net.*"%>


<%!	private ReferenceListHelper rLH = new ReferenceListHelper(); 
	private List<Reference> references; 
%>

<%
//Locates the property file
InputStream stream = application.getResourceAsStream("resources/properties/ServerInfo.properties");
Properties props = new Properties();
props.load(stream);
//Picks up a value
String ADDRESS = props.getProperty("address");

String referencesXML = rLH.referencesXml();
InputStream xsltFile = application.getResourceAsStream("WEB-INF/xslTransformer/transform.xslt");
//create the object from POJO class
XslTransformer xslTrans = new XslTransformer();
%>



<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=ADDRESS%>">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Riferencio</title>
<link rel="stylesheet" href="resources/css/style.css" type="text/css" />

</head>

<body>
	<div id="wrapper">
		<div id="rightcontainer">
			<div id="header">
				<div id="logo">
					<a href="<%=ADDRESS%>"> <img src="resources/images/logo.png" />
					</a>
				</div>
				<!-- end of logo -->
			</div>
			<!-- end of header  -->

			<div class="clear"></div>
			<!-- clears elements -->

			<div id="leftCol">
				<div id="typeMenu">
					<ul>
						<li class="book"><a href="book">Book</a>
						</li>
						<li class="journal"><a href="journal">Journal</a>
						</li>
						<li class="other"><a href="client">SOAP</a>
						</li>
					</ul>
				</div>
				<!-- end of typeMenu -->
			</div>
			<!-- end of leftCol -->


			<div id="rightCol">
				<div id="referenceList">
					<h1 class="alt">References</h1>

					<!-- Print References -->
					<%
					    List<Reference> references = new ReferenceListHelper().references();
						if (references.isEmpty())
							out.println("<div class='systemMessage'>There are no references to display.</div>");
						else
						{
							xslTrans.xmlStyleTransformaton(referencesXML, out, xsltFile); %>

					<div class="download">
						<a href="pdf/download">Download PDF</a>
					</div>

					<% } %>


				</div>
				<!-- end of referenceList -->
			</div>
			<!-- end of rightCol -->
			<div id="middleCol">
				<div id="content">