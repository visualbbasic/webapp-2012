<thead id="authors">
	<tr id="authorControls">
		<td colspan="2">
			<h2 class="formTitle">Author(s)</h2> <input type="button"
			onclick="addRow('authors')" title="Add author input row"
			class="button_add" /> <input type="button"
			onclick="removeRow('authors')" title="Remove author input row"
			class="button_remove" />
		</td>
		<td class="systemMessage"><form:errors path="authors" />
		</td>
	</tr>

	<c:forEach items="${reference.authors}" var="author" varStatus="loop">
		<tr id="rowId${loop.count-0}">
			<td><form:label path="authors[${loop.count-1}].surname">Last Name</form:label>
				<form:input path="authors[${loop.count-1}].surname" />
			</td>
			<td><form:label path="authors[${loop.count-1}].initial">Initial(s)</form:label>
				<form:input path="authors[${loop.count-1}].initial" />
			</td>
			<td class="systemMessage author"><form:errors
					path="authors[${loop.count-1}].surname" /><br> <form:errors
					path="authors[${loop.count-1}].initial" />
			</td>
		</tr>
	</c:forEach>
</thead>