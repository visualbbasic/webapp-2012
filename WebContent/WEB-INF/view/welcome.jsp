<%@page
	import="com.myref.mvc.helper.ReferenceListHelper,com.myref.mvc.model.dao.ReferenceList,com.myref.mvc.harvard.utility.*,java.util.List,com.myref.mvc.model.Reference,com.myref.mvc.model.dao.ReferenceListXmlImpl"%>

<%@include file="includes/header.jsp"%>
<h1>Welcome</h1>
<p>This website is a tool for saving references. Either a book or
	Journal can be added to the list of references by clicking the relevant
	button on the menu.</p>
<p>A SOAP client has been provided for testing and you can download
	a PDF version of your saved references.</p>
<%@include file="includes/footer.jsp"%>