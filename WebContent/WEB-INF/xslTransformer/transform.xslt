<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns="http://www.w3.org/TR/xhtml1/strict">
	<xsl:output method="html" indent="yes" />

	<!-- This XSL Styler is used to transform the list of reference that we 
		want to display after it has been downloaded from the server. The styler 
		starts by matching the root of the XML file and than start to apply-template 
		to match all the elements within the channel element. Some elements can display 
		link and they need to associate an attribute to generate these type of text 
		on the browser. The approach of this styler is using a for-each to loop through 
		the references. -->

	<!--Match root element -->
	<xsl:template match="/">
		<xsl:apply-templates />
	</xsl:template>

	<!--Match reference element -->
	<xsl:template match="reference">
		<div id="xslContainer">
			<div id="xslEdit">
				<a>
					<xsl:attribute name="href">
						<xsl:value-of select="@type" />/edit?refId=<xsl:value-of
						select="@id" />
					</xsl:attribute>
					<img src="resources/images/edit.png" border="0" />
				</a>
			</div>
			<div id="xslDelete">
				<a id="xslDelete">
					<xsl:attribute name="href">
							<xsl:value-of select="@type" />/delete?refId=<xsl:value-of
						select="@id" />
						</xsl:attribute>
					<img src="resources/images/delete.png" border="0" />
				</a>
			</div>

			<div id="xslBody">
				<a id="xslView">
					<xsl:attribute name="href">
				<xsl:value-of select="@type" />/view?refId=<xsl:value-of
						select="@id" />
			</xsl:attribute>
					<xsl:for-each select=".">
						<xsl:for-each select="authors/author">
							<xsl:value-of select="surname" />
							<xsl:text>, </xsl:text>
							<xsl:value-of select="initial" />
							<xsl:choose>
								<xsl:when test="position()=last()-1">
									<xsl:text> &amp; </xsl:text>
								</xsl:when>
								<xsl:when test="position()=last()">
									<xsl:text>&#x00A0;</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>, </xsl:text>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
						<xsl:value-of select="year" />
						<xsl:text>, </xsl:text>
						<xsl:choose>
							<xsl:when test="@type = 'book'">
								<span class="referenceItalic">
									<xsl:value-of select="title" />
								</span>
								<xsl:text>, </xsl:text>
								<xsl:value-of select="edition" />
								<xsl:text>, </xsl:text>
								<xsl:value-of select="publisher" />
								<xsl:text>, </xsl:text>
								<xsl:value-of select="place" />
								<xsl:text>. </xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>'</xsl:text>
								<xsl:value-of select="title" />
								<xsl:text>', </xsl:text>
								<span class="referenceItalic">
									<xsl:value-of select="journal" />
								</span>
								<xsl:text>, vol. </xsl:text>
								<xsl:value-of select="volume" />
								<xsl:text>, no. </xsl:text>
								<xsl:value-of select="issue" />
								<xsl:text>, </xsl:text>
								<xsl:choose>
									<xsl:when test="endPage = ''">
										<xsl:text>p. </xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text>pp. </xsl:text>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:value-of select="startPage" />
								<xsl:choose>
									<xsl:when test="endPage = ''">
										<xsl:text></xsl:text>
									</xsl:when>
									<xsl:otherwise>
										<xsl:text> - </xsl:text>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:value-of select="endPage" />
								<xsl:text>.</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each>
				</a>
			</div>
		</div>
	</xsl:template>
</xsl:stylesheet>