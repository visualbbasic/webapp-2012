<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:dc="http://purl.org/dc/elements/1.1/" version="1.1"
	exclude-result-prefixes="fo">

	<xsl:output method="xml" version="1.0" omit-xml-declaration="no"
		indent="yes" />

	<!-- This XSL-FO Styler is used to transform the list of reference that 
		we want to display to PDF. The styler starts by matching the root of the 
		XML file and than start to apply-template to match all the elements within 
		the channel element. The approach of this styler is using a for-each to loop 
		through the references. The xsl-fo styler sets the size of the pdf page with 
		desired boarder than will model the flow of the page including the footer 
		and the body. -->

	<!--Match root element -->
	<xsl:template match="/">
		<fo:root>

			<!--Sets layout page -->
			<fo:layout-master-set>
				<fo:simple-page-master master-name="A4"
					page-width="210mm" page-height="297mm" margin-top="1cm"
					margin-bottom="1cm" margin-left="1cm" margin-right="1cm">
					<fo:region-body margin="1cm" />
					<fo:region-before extent="2cm" />
					<fo:region-after extent="2cm" />
					<fo:region-start extent="1cm" />
					<fo:region-end extent="1cm" />
				</fo:simple-page-master>
			</fo:layout-master-set>

			<!--Sets footer region -->
			<fo:page-sequence master-reference="A4">

				<!--Sets header contents and layout -->
				<fo:static-content flow-name="xsl-region-before">
					<fo:block font-size="10pt" text-align="right" font-style="italic"
						font-family="Times">
						<fo:block xmlns:date="http://exslt.org/dates-and-times">
							<xsl:variable name="now" select="date:date-time()" />
							<xsl:value-of select="date:day-in-month($now)" />
							<xsl:text> </xsl:text>
							<xsl:value-of select="date:month-name($now)" />
							<xsl:text> </xsl:text>
							<xsl:value-of select="date:year($now)" />
							<xsl:text>, </xsl:text>
							<xsl:value-of select="date:hour-in-day($now)" />
							<xsl:text>:</xsl:text>
							<xsl:value-of select="date:minute-in-hour($now)" />
							<xsl:text>:</xsl:text>
							<xsl:value-of select="date:second-in-minute($now)" />
						</fo:block>
					</fo:block>
				</fo:static-content>

				<!--Sets footer contents and layout -->
				<fo:static-content flow-name="xsl-region-after">
					<fo:block font-size="10pt" text-align="right" font-style="italic"
						border-before-style="inset" border-before-width="thin"
						font-family="Times">
						<fo:block space-before="5mm">
							<fo:table table-layout="fixed" width="100%">
								<fo:table-body table-layout="fixed">
									<fo:table-row line-height="8mm">
										<fo:table-cell text-align="left">
											<fo:block>UTS Harvard Referencing Style</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="right">
											<fo:block>
												page /
												<fo:page-number />
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
						</fo:block>
					</fo:block>
				</fo:static-content>

				<!--Sets title content and layout -->
				<fo:flow flow-name="xsl-region-body">
					<fo:block background-color="#365572" font-weight="bold"
						color="#efe8e0" font-size="18pt" font-family="serif" space-after="10mm"
						line-height="14mm" text-align="center">Riferimento</fo:block>
					<fo:block text-align="left" font-size="12pt" font-family="serif"
						initial-page-number="1">
						<xsl:for-each select="//reference">
							<xsl:sort select="authors/author/surname" />
							<xsl:choose>
								<xsl:when test="position()=last()">
									<fo:block space-after="5mm">
										<xsl:apply-templates select="." />
									</fo:block>
								</xsl:when>
								<xsl:otherwise>
									<fo:block border-after-style="inset"
										border-after-width="thin" space-after="5mm" border-color="#365572">
										<xsl:apply-templates select="." />
									</fo:block>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</fo:block>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>

	<!--Sets body contents and layout -->
	<xsl:template match="reference">
		<xsl:for-each select=".">
			<fo:block start-indent="1cm" text-indent="-1cm" space-after="5mm"
				line-height="6mm">
				<xsl:for-each select="authors/author">
					<xsl:value-of select="surname" />
					<xsl:text>, </xsl:text>
					<xsl:value-of select="initial" />
					<xsl:choose>
						<xsl:when test="position()=last()-1">
							<xsl:text> &amp; </xsl:text>
						</xsl:when>
						<xsl:when test="position()=last()">
							<xsl:text>&#x00A0;</xsl:text>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text>, </xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
				<xsl:value-of select="year" />
				<xsl:text>, </xsl:text>
				<xsl:choose>
					<xsl:when test="@type = 'book'">
						<fo:inline font-style="italic">
							<xsl:value-of select="title" />
						</fo:inline>
						<xsl:text>, </xsl:text>
						<xsl:value-of select="edition" />
						<xsl:text>, </xsl:text>
						<xsl:value-of select="publisher" />
						<xsl:text>, </xsl:text>
						<xsl:value-of select="place" />
						<xsl:text>.</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>'</xsl:text>
						<xsl:value-of select="title" />
						<xsl:text>', </xsl:text>
						<fo:inline font-style="italic">
							<xsl:value-of select="journal" />
						</fo:inline>
						<xsl:text>, vol. </xsl:text>
						<xsl:value-of select="volume" />
						<xsl:text>, no. </xsl:text>
						<xsl:value-of select="issue" />
						<xsl:text>, </xsl:text>
						<xsl:choose>
							<xsl:when test="endPage = ''">
								<xsl:text>p. </xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text>pp. </xsl:text>
							</xsl:otherwise>
						</xsl:choose>
						<xsl:value-of select="startPage" />
						<xsl:choose>
							<xsl:when test="endPage = ''">
								<xsl:text>.</xsl:text>
							</xsl:when>
							<xsl:otherwise>
								<xsl:text> - </xsl:text>
								<xsl:value-of select="endPage" />
								<xsl:text>.</xsl:text>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:otherwise>
				</xsl:choose>
			</fo:block>
			<xsl:if test="url != ''">
				<fo:block space-before="5mm" space-after="2mm">
					Link:
					<fo:inline color="blue" text-decoration="underline">
						<xsl:value-of select="url" />
					</fo:inline>
				</fo:block>
			</xsl:if>
			<xsl:if test="comment != ''">
				<fo:block>
					<fo:table table-layout="fixed" space-after="5mm" width="100%">
						<fo:table-body table-layout="fixed" text-align="left">
							<xsl:for-each select="comment">
								<fo:table-row line-height="8mm">
									<xsl:choose>
										<xsl:when test="position() > 1">
											<fo:table-cell width="70">
												<fo:block></fo:block>
											</fo:table-cell>
										</xsl:when>
										<xsl:otherwise>
											<fo:table-cell width="70">
												<fo:block>Comments: </fo:block>
											</fo:table-cell>
										</xsl:otherwise>
									</xsl:choose>
									<fo:table-cell>
										<fo:block>
											<xsl:apply-templates select="." />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
							</xsl:for-each>
						</fo:table-body>
					</fo:table>
				</fo:block>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>