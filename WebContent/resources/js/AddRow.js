/**
 * 
 */
function addRow(divName){ 
	var counter = count(divName);
	var newrow = document.createElement('tr');
	
	var newsurname = document.createElement('td');
	newsurname.innerHTML='<label for="authors'+ counter +'.surname">Last Name</label>\
						  <input id="authors'+ counter +'.surname" name="authors['+ counter +'].surname" type="text" type="text" value=""/>';
	
	var newinitial = document.createElement('td');
	newinitial.innerHTML='<label for="authors'+ counter +'.initial">Initial(s)</label>\
						  <input id="authors'+ counter +'.initial" name="authors['+ counter +'].initial" type="text" value=""/>';

	// append
	newrow.appendChild(newsurname);
	newrow.appendChild(newinitial);
	
	var newbutton = document.createElement('td');
	newbutton.innerHTML='&nbsp;';
	newrow.appendChild(newbutton);
	
	document.getElementById(divName).appendChild(newrow);
}

function removeRow(parentName){
	var row = document.getElementById(parentName).lastChild;
	var parent = document.getElementById(parentName);
	if(row.id != "authorControls" && count(parentName) != 1)
		parent.removeChild(row);
}

function count(node){
	var count = 0;
	var parent = document.getElementById(node);
	var children = parent.getElementsByTagName("tr");
	for (var i=0;i<children.length;i++){
		count ++;	
	}
	return (count-1);
}

