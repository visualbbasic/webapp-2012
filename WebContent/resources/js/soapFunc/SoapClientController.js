//TextBox class
(function(window) {
	function SoapClientController(textArea) {
		this.ajax = new AjaxReader();
		this.textArea = textArea;
	}
	
	
	//Read xml file from the destination
	SoapClientController.prototype.init = function(url) {
		this.xml = this.ajax.readFile(url);
	};

	SoapClientController.prototype.resetTextArea = function() {
		this.textArea.value = this.xml;
	};
	
	SoapClientController.prototype.initTextArea = function() {
		
	};
	
	window.SoapClientController = SoapClientController;

}(window));
