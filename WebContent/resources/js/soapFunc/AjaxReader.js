(function(window) {
	function AjaxReader() {
	}

	AjaxReader.prototype.readFile = function(url) {
		this.xmlhttp;
		if (window.XMLHttpRequest) {
			//For Rest of the major browsers
			xmlhttp = XMLHttpRequest();
		} else {
			//For IE
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		//Read XML from server.
		xmlhttp.open('GET', url, false);
		xmlhttp.send();
		xmlDoc = xmlhttp.responseXML;
		//Converting XML Element to string
		return (new XMLSerializer()).serializeToString(xmlDoc);
	};
	
	AjaxReader.prototype.readReferencesFile = function(url) {
		this.xmlhttp;
		if (window.XMLHttpRequest) {
			//For Rest of the major browsers
			xmlhttp = XMLHttpRequest();
		} else {
			//For IE
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		//Read XML from server.
		xmlhttp.open('GET', url, false);
		xmlhttp.send();
		xmlDoc = xmlhttp.responseXML;
		//Converting XML Element to string
		return (new XMLSerializer()).serializeToString(xmlDoc);
	};

	window.AjaxReader = AjaxReader;

}(window));
