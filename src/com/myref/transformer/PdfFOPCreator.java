package com.myref.transformer;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;

import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.Transformer;
import javax.xml.transform.Result;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;

/**
 * A Java class application that takes care of all the transformation codes
 * to create pdf file
 * 
 * @author Enrico Caporali
 * 
 * @version 1.0
 * @since 1.0
 * 
 */
public class PdfFOPCreator {
	
	
	/**
	 * This method create the conversion from xml to pdf using a styler called xsl-fo
	 * from the Apache library. This powerful method needs a byte array as input stream 
	 * to make the conversion and a response object to the client to let this happen.
	 * 
	 * @param xml string object from the reference list
	 * @param xslt input stream for as styler
	 * @param pdf file name pdf
	 * @param response servlet to provide the response back to the client
	 */
	public void create (String xml, InputStream xslt, String pdf, HttpServletResponse response)
	 {
		 try {
	         System.out.println("FOP\n");
	         System.out.println("Preparing...");

	         System.out.println("Output: PDF ");
	         System.out.println();
	         System.out.println("Transforming...");
	         
	         //converting XML string object in input stream
	         InputStream xmlIs = new ByteArrayInputStream(xml.getBytes("UTF-8"));
	         
	         // configure fopFactory as desired
	         final FopFactory fopFactory = FopFactory.newInstance();
	
	         // configure foUserAgent as desired
	         FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
	         
	         //creates the output in bytes
	         ByteArrayOutputStream out = new ByteArrayOutputStream();
	
	         try {	         
		         
	        	 // the XML file from which we take the name
	   			 StreamSource source = new StreamSource(xmlIs);
	   			 // creation of transform source
	   			 StreamSource xsltSource = new StreamSource(xslt);
	   			 
	   			 //new factory transformer
	   			 Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);
	             
	             TransformerFactory factory = TransformerFactory.newInstance();
	             
	             //include xsl-fo source
	             Transformer transformer = factory.newTransformer(xsltSource);	
	             
	             // Set the value of a <param> in the stylesheet
	             transformer.setParameter("versionParam", "2.0");

	             // Resulting SAX events (the generated FO) must be piped through to FOP
	             Result res = new SAXResult(fop.getDefaultHandler());
	
	             // Start XSLT transformation and FOP processing
	             transformer.transform(source, res);
	             
	             //Prepare response
	             response.setContentType("application/pdf");
	             response.setContentLength(out.size());
	             response.setHeader("content-disposition", "attachment;filename="+pdf);
	             //response.setHeader("content-disposition", "inline;filename="+pdf);
	             
	             //Send content to Browser
                 response.getOutputStream().write(out.toByteArray());
                 response.getOutputStream().flush();
                
                 
	         }    
	         catch (TransformerException e) {
		 			throw e;
		 		}
	         catch (FOPException e) 
			 {
		 		throw e;
			 }
	         
	         System.out.println("Success!");
	         
		 }catch (Exception e) {
	         e.printStackTrace(System.err);
	         System.exit(-1);
	     }

		 catch (TransformerFactoryConfigurationError e)
		 {
		 	throw e;
		 }
	}
}
