package com.myref.transformer;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.servlet.jsp.JspWriter;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 * A Java class application that takes care of all the transformation codes
 * to display the xml reference list to the browser
 * 
 * @author Enrico Caporali
 * 
 * @version 1.0
 * @since 1.0
 * 
 */
public class XslTransformer {
	
	/**
	 * This method is used to transform the xml reference list that has been
	 * created and associate a XSLT styler with it to give
	 * the possibility of styling the results back to the server.
	 * 
	 * @param isXml
	 *            this parameter consent to input the xml reference list we want to convert
	 * @param out
	 *            this parameter provide an output JspWriter to the web
	 *            browser
	 * @param is
	 *            this parameter consent to retrieve the XSLT file from the
	 *            local file
	 * @return this method returns a string which is the StreamResults that has
	 *         been transformed and converted in to a string to pass it to the
	 *         browser.
	 */
	public String xmlStyleTransformaton(String isXml, JspWriter out, InputStream is) {
		try {
			InputStream xmlIs = new ByteArrayInputStream(isXml.getBytes("UTF-8"));

			// Load StreamSource objects with XML and XSLT files
			StreamSource xmlSource = new StreamSource(xmlIs);
			StreamSource xsltSource = new StreamSource(is);

			// Set StreamResult objects to a PrintWriter
			StreamResult fileResult = new StreamResult(out);
			// Load a Transformer object and perform the transformation
			TransformerFactory tfFactory = TransformerFactory.newInstance();
			// Associate the TransformerFactory with the XSLT styler
			Transformer tf = tfFactory.newTransformer(xsltSource);
			// Process the actual transformation
			tf.transform(xmlSource, fileResult);
			// Convert the result file into a string
			String mys = fileResult.toString();

			// return my string
			return mys;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
}
