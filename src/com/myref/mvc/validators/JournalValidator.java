package com.myref.mvc.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.myref.mvc.constraints.Journal;
import com.myref.mvc.model.Reference;

/**
 * Checks if the journal object has required fields.
 * @author Tomato
 *
 */
public class JournalValidator implements ConstraintValidator<Journal, Reference> {

    private String type;
    private String journal;
    private String volume;
    private String issue;
    private String startPage;
    
    @Override
	public void initialize(Journal constraintAnnotation) {}

    public boolean isValid(Reference object, ConstraintValidatorContext constraintContext) {
    	
    	if (object == null)
            return false;
    	else 
    		setup(object);
    	
    	if (journal() && fieldsNull())
    		return false;
    	
    	if (journal() && fieldsEmpty())
    		return false;
    	
        return true;
    }
    
    /**
     * Stores an objects attributes as local attributes.
     * @param reference
     */
    private void setup(Reference object){
    	this.type = object.getType();
    	this.journal = object.getJournal();
    	this.volume = object.getVolume();
    	this.issue = object.getIssue();
    	this.startPage = object.getStartPage();
    }
    
    /** Checks that fields required in journal are not null
     * @return boolean
     */
    private boolean fieldsNull(){
    	return isNull(journal) || isNull(volume) || isNull(issue) || isNull(startPage);
    }
    
    /** Checks that fields required in journal are not empty
     * @return boolean
     */
    private boolean fieldsEmpty(){
    	return journal.isEmpty() || volume.isEmpty() || issue.isEmpty() || startPage.isEmpty();
    }
    
    /**
     * returns a fields null status
     * @param field
     * @return
     */
    private boolean isNull(String field){
    	return field == null;
    }
    
    /**
     * checks if the object is a journal.
     * @return true if references type is journal.
     */
    private boolean journal(){
    	return type.equalsIgnoreCase("JOURNAL");
    }
}
