package com.myref.mvc.validators;

import com.myref.mvc.model.Author;
import com.myref.mvc.model.Reference;

/**
 * @author Jason Shin
 *	Generate reference validation error into unordered list in HTML
 */
public class ReferenceValidator {

	
	/**
	 * @param reference
	 * validates each individual reference fields. replaces all unnecessary lists.
	 * @return validation results
	 */
	public String validate(Reference reference){
		String finalResult = "";
		StringBuffer sb = new StringBuffer();
		sb.append("<ul>");
		sb.append("<li>" + this.validateAuthors(reference.getAuthors()) + "</li>");
		sb.append("<li>" + this.validateYear(reference.getYear()) + "</li>");
		sb.append("<li>" + this.validateTitle(reference.getTitle()) + "</li>");
		sb.append("<li>" + this.validatePublisher(reference.getPublisher()) + "</li>");
		if(reference.getType() != null && reference.getType().equals("journal")){
			sb.append("<li>" + this.validateJournal(reference.getJournal()) + "</li>");
			sb.append("<li>" + this.validateVolume(reference.getVolume()) + "</li>");
			sb.append("<li>" + this.validateIssue(reference.getIssue()) + "</li>");
		} else {
			sb.append("<li>" + this.validateEdition(reference.getEdition()) + "</li>");
			sb.append("<li>" + this.validatePlace(reference.getPlace()) + "</li>");
		}
		sb.append("<li>" + this.validateURL(reference.getUrl()) + "</li>");
		sb.append("</ul>");
		finalResult = sb.toString().replaceAll("<li>null</li>", "").replaceAll("<li></li>", "");
		return finalResult;
	}
	
	
	/**
	 * @param thing
	 *  Validates URL.
	 *  There are three main validate operations processes for this method.
	 *  1) Checks if the string is null.
	 *  2) Checks if the String is empty
	 *  3) Checks if the string is ""
	 * @return URL validation result
	 */
	private String validateURL(String thing){
		String newThing = thing;
		return (newThing == null || newThing.trim().isEmpty()|| newThing.trim() == "") ? "URL is empty" : null; 
	}
	
	
	/**
	 * @param thing
	 *  Validates place.
	 *  There are three main validate operations processes for this method.
	 *  1) Checks if the string is null.
	 *  2) Checks if the String is empty
	 *  3) Checks if the string is ""
	 * @return Place validation result
	 */
	private String validatePlace(String thing){
		String newThing = thing;
		return (newThing == null || newThing.trim().isEmpty()|| newThing.trim() == "") ? "Place of publication is empty" : null; 
	}
	
	/**
	 * @param thing
	 *  Validates issue.
	 *  There are three main validate operations processes for this method.
	 *  1) Checks if the string is null.
	 *  2) Checks if the String is empty
	 *  3) Checks if the string is ""
	 * @return issue validation result
	 */
	private String validateIssue(String thing){
		String newThing = thing;
		return (newThing == null || newThing.trim().isEmpty()|| newThing.trim() == "") ? "Issue is empty" : null; 
	}
	
	/**
	 * @param thing
	 *  Validates volume.
	 *  There are three main validate operations processes for this method.
	 *  1) Checks if the string is null.
	 *  2) Checks if the String is empty
	 *  3) Checks if the string is ""
	 * @return Volume validation result
	 */
	private String validateVolume(String thing){
		String newThing = thing;
		return (newThing == null || newThing.trim().isEmpty()|| newThing.trim() == "") ? "Volume is empty" : null; 
	}
	
	/**
	 * @param thing
	 *  Validates Journal.
	 *  There are three main validate operations processes for this method.
	 *  1) Checks if the string is null.
	 *  2) Checks if the String is empty
	 *  3) Checks if the string is ""
	 * @return Journal validation result
	 */
	private String validateJournal(String thing){
		String newThing = thing;
		return (newThing == null || newThing.trim().isEmpty()|| newThing.trim() == "") ? "Journal is empty" : null; 
	}

	
	/**
	 * @param thing
	 *  Validates Title.
	 *  There are three main validate operations processes for this method.
	 *  1) Checks if the string is null.
	 *  2) Checks if the String is empty
	 *  3) Checks if the string is ""
	 * @return Title validation result
	 */
	private String validateTitle(String thing){
		String newThing = thing;
		return (newThing == null || newThing.trim().isEmpty()|| newThing.trim() == "") ? "Title is empty" : null; 
	}
	
	
	/**
	 * @param thing
	 *  Validates publisher.
	 *  There are three main validate operations processes for this method.
	 *  1) Checks if the string is null.
	 *  2) Checks if the String is empty
	 *  3) Checks if the string is ""
	 * @return URL validation publisher
	 */
	private String validatePublisher(String thing){
		String newThing = thing;
		return (newThing == null || newThing.trim().isEmpty()|| newThing.trim() == "") ? "Publisher is empty" : null; 
	}
	
	/**
	 * @param thing
	 *  Validates Edition.
	 *  There are three main validate operations processes for this method.
	 *  1) Checks if the string is null.
	 *  2) Checks if the String is empty
	 *  3) Checks if the string is ""
	 * @return URL validation edition
	 */
	private String validateEdition(String thing){
		String newThing = thing;
		return (newThing == null || newThing.trim().isEmpty()|| newThing.trim() == "") ? "Edition is empty" : null; 
	}
	
	
	/**
	 * @param thing
	 *  Validates year.
	 *  There are three main validate operations processes for this method.
	 *  1) Checks if the string is null.
	 *  2) Checks if the String is empty
	 *  3) Checks if the string is ""
	 * @return URL validation year
	 */
	private String validateYear(int i){
		String year = ""+i;
		String suggestion = "Year cannot be more than 4 digits!";
		String validateYear = "\\d{0,4}";
		if(!year.matches(validateYear)){
			return suggestion;
		}
		return null;
	}
	
	
	
	/**
	 * @param authors
	 * the surname has to have 1 capital letter in the beginning
	 * initials has to be in A. B. or A.B. format.
	 * @return validation result
	 */
	private String validateAuthors(Author[] authors) {
		StringBuffer buffer = new StringBuffer();
		//must start with a captial letter
		String surnamePattern = "^[A-Z]{1}[A-Za-z]{1,}$"; 
		//must have 1 captital letter before every each dot A.B.N.
		String initialPattern = "([A-Z]\\.\\s*){1,}";
		System.out.println("initial: " + authors[0].getInitial().trim());
		for (int i = 0; i < authors.length; i++) {
			if (!authors[i].getInitial().trim().matches(initialPattern)) {
				buffer.append("Author" + (i + 1) + " has an error at initial! Format must be 1 capital letter followed by a dot<br />");
			}
			if(!authors[i].getSurname().trim().matches(surnamePattern)){
				buffer.append("Author" + (i+1) + " has an error at surname! Surname must start with an upper case<br />");
			}
		}
		return buffer.toString();
	}


}
