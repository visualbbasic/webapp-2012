package com.myref.mvc.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.myref.mvc.constraints.Book;
import com.myref.mvc.model.Reference;

/**
 * Checks if the book object has required fields.
 * @author Tomato
 *
 */
public class BookValidator implements ConstraintValidator<Book, Reference> {

    private String type;
    private String place;
    private String edition;
    private String publisher;
    
    @Override
	public void initialize(Book constraintAnnotation){}

    public boolean isValid(Reference object, ConstraintValidatorContext constraintContext) {
    	
    	if (object == null)
            return false;
    	else 
    		setup(object);
    	
    	if (isBook() && fieldsNull())
    		return false;
    	
    	if (isBook() && fieldsEmpty())
    		return false;
    	
        return true;
    }
    
    /**
     * Stores an objects attributes as local attributes.
     * @param reference
     */
    private void setup(Reference object){
    	this.type = object.getType();
    	this.place = object.getPlace();
    	this.edition = object.getEdition();
    	this.publisher = object.getPublisher();
    }
    
    /** Checks that fields required in book are not null
     * @return boolean
     */
    private boolean fieldsNull(){
    	return isNull(place) || isNull(edition) || isNull(publisher);
    }
    
    /** Checks that fields required in book are not empty
     * @return boolean
     */
    private boolean fieldsEmpty(){
    	return place.isEmpty() || edition.isEmpty() || publisher.isEmpty();
    }
    
    
    /**
     * returns a fields null status
     * @param field
     * @return
     */
    private boolean isNull(String field){
    	return field == null;
    }
        
    /**
     * checks if the object is a book.
     * @return true if references type is book.
     */
    private boolean isBook(){
    	return type.equalsIgnoreCase("BOOK");
    }
}
