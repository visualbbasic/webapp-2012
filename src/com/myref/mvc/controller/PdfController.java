package com.myref.mvc.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/** 
 * Maps to pdf download button
 * @author Tomato
 *	
 */
@Controller
public class PdfController {
	
	/**
	 * Handles download requests for the pdf File
	 * @param request
	 * @return jsp of pdf/download
	 */
	@RequestMapping(value = "/pdf/download")
	public ModelAndView pdf(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("pdf/download");
		return mv;
	}
}
