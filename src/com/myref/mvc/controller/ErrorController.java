package com.myref.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *	@author Tomato
 *	Handles 404 and 405 errors
 */
@Controller
public class ErrorController {
	
	/** HttpError 404 handler
	 * @return view with error message
	 */
	@RequestMapping(value = "httpError")
	public ModelAndView http() {
		ModelAndView mv = new ModelAndView("error/404");
		return mv;
	}
	
	/** GetError 405 handler
	 * @return view with error message
	 */
	@RequestMapping(value = "getError")
	public ModelAndView get() {
		ModelAndView mv = new ModelAndView("error/405");
		return mv;
	}
}
