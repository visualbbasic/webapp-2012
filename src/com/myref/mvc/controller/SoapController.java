package com.myref.mvc.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *	@author Tomato
 *	For handling soap client requests.
 */
@Controller
public class SoapController {
	
	/**
	 * @param request
	 * @return	returns soap/soapClient view.
	 */
	@RequestMapping(value = "client")
	public String client(HttpServletRequest request) {
		request.setAttribute("test", "test");
		return "soap/soapClient";
	}

}