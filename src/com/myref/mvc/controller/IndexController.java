package com.myref.mvc.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author Tomato	
 * Site index controller
 * 
 */
@Controller
public class IndexController {

	/** Maps to the websites default page.
	 * @param request
	 * @return welcome view.
	 */
	@RequestMapping(value = "/")
	public ModelAndView welcome(HttpServletRequest request) {
		ModelAndView mv = new ModelAndView("welcome");
		return mv;
	}
}
