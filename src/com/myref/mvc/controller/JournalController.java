package com.myref.mvc.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.myref.mvc.helper.ReferenceListHelper;
import com.myref.mvc.model.Reference;

/**
 * 	@author Tomato
 * 	Controller class for Reference model type Journal
 *	This controller uses the structure type/operation.
 *	Loads the relevant views.
 *	
 */
@Controller
public class JournalController {
	
	/** Default journal controller, maps to journal creation view.
	 * @return journal create view with new reference.
	 */
	@RequestMapping(value = "journal")
	public ModelAndView journal() {
		ModelAndView mv = new ModelAndView("journal/create");
		mv.addObject("reference", new Reference());
		return mv;
	}
	
	
	/** Submit new journal controller
	 * Binds reference from form submission
	 * @param reference, a validated and bound result
	 * @param result of validation
	 * @param model
	 * @return either create or success view depending on errors
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "journal/add", method=RequestMethod.POST)
	public String submit(@Valid Reference reference, BindingResult result, Map model) {
		if (result.hasErrors()) {
            return "journal/create";
		} else {
			try {
				ReferenceListHelper helper = new ReferenceListHelper();
				helper.add(reference);
			} catch (IOException e) {e.printStackTrace();}
		}
		return "journal/success";
	}
	
	/** Submit update journal controller
	 * Binds reference from form submission
	 * @param reference, a validated and bound result
	 * @param result of validation
	 * @param model
	 * @return either update or success view depending on errors
	 */
	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "journal/update", method=RequestMethod.POST)
	public String update(@Valid Reference reference, BindingResult result, Map model) {
		if (result.hasErrors()) {
            return "journal/update";
		} else {
			try {
				ReferenceListHelper helper = new ReferenceListHelper();
				helper.update(reference.getId(), reference);
			} catch (IOException e) {return "welcome";}
		}
		return "journal/success";
	}
	
	/** Deletes journal based on id
	 * @param request, parameter refId
	 * @return delete page or not found
	 */
	@RequestMapping(value = "journal/delete")
	public ModelAndView delete(HttpServletRequest request) {
		// Setup
		ModelAndView mv = new ModelAndView("journal/delete");
		ReferenceListHelper helper = new ReferenceListHelper();
		
		// Lookup
		int id = Integer.parseInt(request.getParameter("refId"));
		Reference reference = helper.reference(id);
		
		// Check result
		if (reference == null) {
			return new ModelAndView("error/404");
		} else {
			try {
				mv.addObject("reference", helper.reference(id));
				helper.remove(id);
			} catch (IOException e) {e.printStackTrace(); } }
		return mv;
	}
	
	/** Lookup and Edit reference
	 * @param request reference id refId
	 * @return update view or 404 error if reference not found
	 */
	@RequestMapping(value = "journal/edit")
	public ModelAndView edit(HttpServletRequest request) {
		// Setup
		ModelAndView mv = new ModelAndView("journal/update");
		ReferenceListHelper helper = new ReferenceListHelper();
		
		// Lookup
		int id = Integer.parseInt(request.getParameter("refId"));
		Reference reference = helper.reference(id);
		
		// Check result
		if (reference == null)
			return new ModelAndView("error/404");
		else 
			mv.addObject("reference", reference);
		return mv;
	}
	
	/** Lookup and display a reference
	 * @param request reference id, refId
	 * @return
	 */
	@RequestMapping(value = "journal/view")
	public ModelAndView view(HttpServletRequest request) {
		// Setup
		ModelAndView mv = new ModelAndView("journal/view");
		ReferenceListHelper helper = new ReferenceListHelper();
		
		// Lookup
		int id = Integer.parseInt(request.getParameter("refId"));
		Reference reference = helper.reference(id);
		
		// Check result
		if (reference == null)
			return new ModelAndView("error/404");
		else 
			mv.addObject("reference", reference);
		return mv;
	}
}
