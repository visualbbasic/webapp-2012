package com.myref.mvc.model;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.format.annotation.NumberFormat.Style;

import com.myref.mvc.constraints.*;

/** 
 * This is the object for reference, it stores attributes of a references as well as a reference type.
 * Validation is performed for each type.
 * @author Tomato
 *
 */
//Type validation
@Book(value = "Book", message = "Edition, Publisher and Place of publication are required")
@Journal(value = "Journal", message = "Journal Name, Volume Number, Issue Number and starting page numbers are required")

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Reference {
	
	@NotEmpty
	@XmlAttribute(name="type")
	private String type;
	@XmlAttribute(name="id")
	private int id;

    @Valid
    @XmlElementWrapper(name="authors")
    @XmlElement(name="author")
	private Author[] authors;
	
	@NotNull
	@Min(0)
	@Max(2012)
	private Integer year;
    
    @NotEmpty
	private String title;
    
    // Type specific fields
	// Book
    private String edition;
	private String publisher;
	private String place;
	
	// Journal
	private String journal;
	private String volume;
	private String issue;
	@NumberFormat(style = Style.NUMBER)
	private String startPage;
	@NumberFormat(style = Style.NUMBER)
	private String endPage;
	
	
	// Optional fields
	private String url;
	private String comment;
	
	/**
	 * Default constructor initializes array and id.
	 * 
	 */
	public Reference(){
		id = 0;
		//year = 1900;
		authors = new Author[1];
		authors[0] = new Author();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public void setId(int id){
		this.id = id;		
	}

	public int getId() {
		return id;
	}
	
	public Author[] getAuthors() {
		return authors;
	}

	public void setAuthors(Author[] authors) {
		this.authors = authors;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getJournal() {
		return journal;
	}

	public void setJournal(String journal) {
		this.journal = journal;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getIssue() {
		return issue;
	}

	public void setIssue(String issue) {
		this.issue = issue;
	}

	public String getStartPage() {
		return startPage;
	}

	public void setStartPage(String startPage) {
		this.startPage = startPage;
	}

	public String getEndPage() {
		return endPage;
	}

	public void setEndPage(String endPage) {
		this.endPage = endPage;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		if (!url.isEmpty())
			this.url = url;
		
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		if (!comment.isEmpty())
			this.comment = comment;
	}
	
	/**
	 * Allows lookup based on id.
	 * @param id
	 * @return boolean of match
	 */
	public boolean matches(int id){
		return this.id == id;		
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if (obj == null) {
			return false;
		}
		
		if (!(obj instanceof Reference)) {
			return false;
		}
		
		Reference other = (Reference) obj;
		if (!matches(other.getId())) {
			return false;
		}
		
		return true;
	}
	
	/** Returns a string of authors separated by & or ,
	 * @return String
	 */
	public String authorsToString(){
		String s = "";
		int count = authors.length;
		if (authors != null)
			for (Author author : authors) {
				s += author.toString();
				if (count == 2 && authors.length > 1)
					s += " &";
				else if (count > 2)
					s += ",";
				count--;
				s += " ";
			}
		return s;
	}
	
	/**
	 * Appends text with delineation if the field is not null.
	 * @param text
	 * @return
	 */
	private String append(String text){
		if(text != null)
			return " " + text + ",";
		return "";
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		String s = "";
		s += authorsToString();
		if(year != 0)
			s += year;
		s += append(title);
		s += append(edition);
		s += append(publisher);
		s += append(journal);
		s += append(volume);
		s += append(issue);
		if(startPage != null)
			s += startPage;
		if(endPage != null)
			s += endPage;
		s += append(place);
		s += append(url);
		s += append(comment);
		return s;
	}
}
