package com.myref.mvc.model.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.myref.mvc.model.Reference;
import com.myref.mvc.model.References;

public class ReferenceListXmlImpl implements ReferenceList {
	
	/**
	 * Internal storage for the list of references.
	 */
	private References references;
	
	/**
	 * An empty constructor, which creates a new ArrayList implementation of a
	 * reference list. The ArrayList is created as a synchronizedList so that if
	 * concurrent access to the same list is required, it is possible as long as
	 * any iterators over the list are placed in a synchronized block code
	 * block.
	 */
	public ReferenceListXmlImpl(){
		super();
		references = new References();		
	}
	
	@Override
	public void add(Reference r) {
		references.add(r);
	}

	@Override
	public void update(Reference oldReference, Reference newReference) {
		references.update(oldReference, newReference);
	}

	@Override
	public void remove(Reference r) {
		references.remove(r);
	}

	@Override
	public List<Reference> list() {
		return references.getReferences();
	}
	
	/**
	 * Loads the persistently stored List of references from an XML file.
	 * The file name is passed into the method which uses JAXB to unmarshal
	 * the XML file into a references object which is stored in the references array ready
	 * for use later. If the file does not exist it is created.
	 * 
	 * @param referenceFile - String name of file to load / create.
	 */
	@Override
	public void load(String referenceFile) throws IOException,
			FileNotFoundException {
		references.clear();
		File referenceFileObj = new File(referenceFile);
		
		// If the file doesn't already exist, create an empty one
		if (!referenceFileObj.exists()) {
			referenceFileObj.createNewFile();
		}
		try {
			InputStream is = new FileInputStream(referenceFile);
			
			JAXBContext jc = JAXBContext.newInstance(References.class);
			Unmarshaller u = jc.createUnmarshaller();
			
			//Create references object
			References referenceList = (References) u.unmarshal(is);
			references = referenceList;
			is.close();
			
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * This implementation saves the list of references into an XML file using
	 * a JAXB marshaller.
	 * 
	 * @see com.myref.mvc.model.dao.ReferenceList#save(java.lang.String)
	 * @param referenceFile - String name of the reference file to save the content in.
	 */
	@Override
	public void save(String referenceFile) throws IOException {
		try {
			JAXBContext jc = JAXBContext.newInstance(References.class);
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			
			References references = this.references;			
			
			// Save File
			m.marshal(references, new FileOutputStream(referenceFile));
			
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}	
}
