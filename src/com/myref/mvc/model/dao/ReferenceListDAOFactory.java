package com.myref.mvc.model.dao;


/**
 * A factory for the data access layer.
 * @author Tomato
 *
 */
public class ReferenceListDAOFactory {
	public ReferenceList newReferenceListDAO(String type) {
		if ("xml".equals(type))
			return new ReferenceListXmlImpl();
		else {
			// Return default
			return new ReferenceListXmlImpl();
		}
	}
}
