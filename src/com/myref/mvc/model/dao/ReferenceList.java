package com.myref.mvc.model.dao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import com.myref.mvc.model.Reference;

/**
 * Handles data access of a list of References.
 * Credit to Wayne Brooks.
 * @author Tomato
 *
 */
public interface ReferenceList {
	
	/**
	 * Adds a new Reference to the list.
	 * 
	 * @param r
	 *            The {@link Reference} to be added to the list
	 */
	public void add(Reference r);

	/**
	 * Searches the list for oldReference, and if found, updates it to newReference.
	 * 
	 * @param oldReference
	 *            The {@link Reference} to be updated. This should already be in the
	 *            list
	 * @param newReference
	 *            The new {@link Reference} to be added to the list in place of
	 *            oldReference
	 */
	public void update(Reference oldReference, Reference newReference);

	/**
	 * Removes an existing Reference from the list. If the Reference does not exist, there
	 * is no error or exception, so this must be handled internally by the
	 * implementation.
	 * 
	 * @param f
	 *            The {@link Reference} to be removed from the list
	 */
	public void remove(Reference r);

	/**
	 * Retrieves a read-only list of all of the Reference currently in the list. The
	 * caller should not try and change this list directly, but use the
	 * add/update/remove methods of this class.
	 * 
	 * @return A read-only list that contains all of the references
	 */
	public List<Reference> list();

	/**
	 * Loads a copy of the reference list from a file.
	 * 
	 * @param referenceFile
	 *             The filename from which to load the list of References.
	 * @throws FileNotFoundException
	 *             If the file referenced by referenceFile does not exist
	 * @throws IOException
	 *             If there was an problem reading from the file
	 */
	public void load(String referenceFile) throws IOException, FileNotFoundException;

	/**
	 * Saves a persistent copy of the reference list into a file.
	 * 
	 * @param referenceFile
	 *             The filename into which to save the list of References.
	 * @throws IOException
	 *             If there was a problem writing to the file
	 */
	public void save(String referenceFile) throws IOException;
}
