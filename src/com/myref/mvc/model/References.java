package com.myref.mvc.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * Model for storage of references.
 * Credit to Wayne Brooks.
 * @author Tomato
 *
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class References {

		
		@XmlElement(name="reference")
		private List<Reference> references;
		
		/**
		 * Default constructor
		 */
		public References(){ 	
			references = Collections.synchronizedList(new ArrayList<Reference>());
		}
		
		/** A Constructor that sets added references to an attribute
		 * @param references - List<Reference>
		 */
		public References(List<Reference> references)
		{	this.references = references;	}

		public List<Reference> getReferences() {
			return Collections.unmodifiableList(references);
		}

		public void setReferences(List<Reference> references) {
			this.references = references;
		}
		
		
		/**
		 * Clears the list of all reference elements.
		 * 
		 */
		public void clear() {
			references.clear();
		}
		
		/**
		 * Adds a new reference to the list.
		 * @param reference
		 */
		public void add(Reference reference) {
			if (!exists(reference)) {
				references.add(reference);
			}
		}
		
		/**
		 * Removes a reference from the list.
		 * @param r
		 */
		public void remove(Reference r) {
			if (indexOf(r) >= 0) {
                references.remove(indexOf(r));
			}
		}
		
		/**
		 * Updates a reference
		 * @param oldReference
		 * @param newReference
		 */
		public void update(Reference oldReference, Reference newReference) {
			if (indexOf(oldReference) >= 0) {
				references.remove(indexOf(oldReference));
				if (!exists(newReference)) {
					references.add(newReference);
				}
			}
		}
		
		/**
		 * A helper method that will search for a specified feed in the list, and if
		 * found, return its position in the list.
		 * 
		 * @param f
		 *            The feed to search for
		 * @return The index of the first occurrence of the Feed f, or -1 if it is
		 *         not found.
		 */
		private int indexOf(Reference r) {
			return references.indexOf(r);
		}

		/**
		 * A helper method that will search for a specified reference in the list, and
		 * return true if found, or false otherwise
		 * 
		 * @param r
		 *            The reference to search for
		 * @return true if the Feed f is found in the list, false otherwise
		 */
		private boolean exists(Reference r) {
			return (indexOf(r) >= 0);
		}
}
