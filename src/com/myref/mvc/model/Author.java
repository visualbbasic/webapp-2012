package com.myref.mvc.model;

import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Model of author data. Is validated using annotations.
 * @author Tomato
 *
 */
@XmlRootElement(name="author")
@XmlAccessorType(XmlAccessType.FIELD)
public class Author {
	
	@Pattern(regexp = "([A-Z]\\.\\s*)*")
	private String initial;
	
	@NotEmpty
	@Pattern(regexp = "(^[A-Z]{1}[a-z]{1,}$)*")
	private String surname;
	
	
	/**
	 * Inits to empty strings. 
	 */
	public Author(){
		initial = "";
		surname = "";
	}
	
	/**
	 * Constructor with surname and initial.
	 * @param surname
	 * @param initial
	 */
	public Author(String surname, String initial){
		setSurname(surname);
		setInitial(initial);
	}
	
	public String getInitial() {
		return initial;
	}

	public void setInitial(String initial) {
		this.initial = initial;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		String s = "";
		if (surname != null){	
			s += surname;
			if(initial != null)
				s += ", " + initial; 
		}
		return s;
	}

}
