package com.myref.mvc.harvard.utility;

import com.myref.mvc.model.Author;
import com.myref.mvc.model.Reference;

/**
 * @author Jason Shin 
 * 
 * ReferenceUtils to convert a reference object to havard
 *         referencing style.
 */
//This is just for a testing purpose until I get things working properly.
public class HavardReference {

	public static String TYPE_BOOK = "book";
	public static String TYPE_JOURNAL = "journal";

	/**
	 * @param reference
	 * @return
	 */
	public static Reference quickFix(Reference reference) {
		return null;
	}

	/**
	 * @param reference
	 * @return
	 */
	public static String warningFinder(Reference reference) {
		return "";
	}

	/**
	 * @param reference
	 * @param type
	 * @return
	 */
	public static String convert(Reference reference, String type) {
		StringBuffer sb = new StringBuffer();
		if (TYPE_BOOK.equalsIgnoreCase(type)) {
			sb.append(authorNameBuilder(reference.getAuthors())
					+ reference.getYear() + ", " + "<i>" + reference.getTitle()
					+ "</i>, " + reference.getEdition() + ", "
					+ reference.getPublisher() + ", " + reference.getPlace()
					+ ".");

		} else {
			sb.append(authorNameBuilder(reference.getAuthors())
					+ reference.getYear() + ", " + "'" + reference.getTitle()
					+ "', " + "<i>" + reference.getJournal() + "</i>, "
					+ " vol. " + reference.getVolume() + ", " + " no. "
					+ reference.getIssue() + "pp. " + reference.getStartPage()
					+ "-" + reference.getEndPage()
			);
		}
		return sb.toString();
	}

	/**
	 * @param authors
	 * @return
	 */
	private static String authorNameBuilder(Author[] authors) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < authors.length; i++) {
			if (i == authors.length - 2) {
				sb.append(authors[i].getSurname() + " "
						+ authors[i].getInitial() + " & ");
				continue;
			} else if (i == authors.length - 1) {
				sb.append(authors[i].getSurname() + " "
						+ authors[i].getInitial() + " ");
			} else {
				sb.append(authors[i].getSurname() + " "
						+ authors[i].getInitial() + ", ");
			}
		}

		return sb.toString();
	}

}
