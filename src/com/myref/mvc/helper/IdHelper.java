package com.myref.mvc.helper;

import java.io.*;

/**
 * This helper handles generation of a unique ID based on a stored integer.
 * @author Tomato
 */
public class IdHelper {
	
	private String fileName;
	
	/** 
	 * Constructor, default filename 
	 */
	public IdHelper(){
		this.fileName = "uniqueId.data";		
	}
	
	/**
	 * Constructor, different filename
	 * @param fileName
	 */
	public IdHelper(String fileName){
		this.fileName = fileName;
	}
	
	/**
	 * Generate unique Id, saves it and returns it.
	 * @return integer id
	 */
	public int getUniqueId(){
		int lastId = 0;
		
		// Get Last ID
		try {
			lastId = getLastId();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// Make new ID
		int uniqueId = lastId + 1;
		
		// Save latest ID
		try {
			saveUniqueId(uniqueId);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return uniqueId; // return unique ID
	}
	
	/** 
	 * gets most recently saved id
	 * @return
	 * @throws IOException
	 */
	private int getLastId() throws IOException{
		
		// Create and check if file exists
		File inFile = new File(this.fileName);
		if (!inFile.exists()) {
			// Create new file and save int 0 to it.
			inFile.createNewFile();
			saveUniqueId(0);
		}
		
		FileInputStream inFileStream = new FileInputStream(inFile);
		DataInputStream inDataStream = new DataInputStream(inFileStream);
		int lastID = inDataStream.readInt();
		inDataStream.close();
		
		return lastID; // return last id that was saved to file.
	}
	
	/** 
	 * Saves unique Id to the file system
	 * @param uniqueId
	 * @throws IOException
	 */
	private void saveUniqueId(int uniqueId) throws IOException{
		File outFile = new File(this.fileName);
		FileOutputStream outFileStream = new FileOutputStream(outFile);
		DataOutputStream outDataStream = new DataOutputStream(outFileStream);
		outDataStream.writeInt(uniqueId);
		outDataStream.close();
	}
}
