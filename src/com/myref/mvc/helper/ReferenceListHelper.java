/**
 * 
 */
package com.myref.mvc.helper;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.myref.mvc.model.Reference;
import com.myref.mvc.model.References;
import com.myref.mvc.model.dao.ReferenceList;
import com.myref.mvc.model.dao.ReferenceListDAOFactory;

/**
 * Helps with persistent storage of reference.
 * Allows for easy implementation of a different storage solution.
 * Handles loading and saving.
 * @author Tomato
 *
 */
public class ReferenceListHelper {
	
	private ReferenceListDAOFactory f = new ReferenceListDAOFactory();
	private ReferenceList rL = f.newReferenceListDAO("xml");
	private String feedFile = "myreferences.xml";
	
	/**
	 * Adds a persistently stored reference with an automatically generated ID.
	 * @param reference
	 * @throws IOException
	 */
	public void add(Reference reference) throws IOException{
		load();
		// Add unique ID to reference.
		IdHelper iH = new IdHelper();
		reference.setId(iH.getUniqueId());
		
		// Add and save
		rL.add(reference);
		save();
	}
	
	/**
	 * Removes a persistently stored reference based on its id.
	 * @param id
	 * @throws IOException
	 */
	public void remove(int id) throws IOException{
		rL.remove(reference(id));
		save();
	}
	

	/**
	 * Updates a persistently stored references.
	 * @param id of reference to update.
	 * @param newReference the reference object to be saved.
	 * @throws IOException
	 */
	public void update(int id, Reference newReference) throws IOException {
		rL.update(reference(id), newReference);
		save();
	}

	/** 
	 * Returns an unmodifiable list of previously saved reference objects.
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public List<Reference> references() throws FileNotFoundException, IOException{
		load();
		return rL.list();
	}
	

	/**
	 * Generates an XML string of the currently saved references.
	 * @return
	 * @throws JAXBException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public String referencesXml() throws JAXBException, FileNotFoundException, IOException {
		JAXBContext jc = JAXBContext.newInstance(References.class);
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		References references = new References(references());			
		StringWriter result = new StringWriter();
		// marshal to string
		m.marshal(references, result);
		return result.toString();		
	}
	
	/** 
	 * Loads references based on a given filename.
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private void load() throws FileNotFoundException, IOException{
		rL.load(feedFile);
	}
	
	/** 
	 * Saves references based on a given filename.
	 * @throws IOException
	 */
	private void save() throws IOException{
		rL.save(feedFile);
	}
	
	/**
	 * Looks up and returns a reference based on the id field.
	 * @param id
	 * @return a reference
	 */
	public Reference reference(int id) {
	try {
		for (Reference reference : references())
			if (reference.matches(id))
				return reference;
	} catch (FileNotFoundException e) {e.printStackTrace();} catch (IOException e) {e.printStackTrace();}
	return null;
	}
}
