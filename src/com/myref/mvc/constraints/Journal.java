package com.myref.mvc.constraints;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.myref.mvc.validators.JournalValidator;

/**
 * @author Tomato
 *	Constraint to check that all required Journal items are filled if reference is a book.
 */
@Target({ TYPE, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = JournalValidator.class)
@Documented
public @interface Journal {

    String message() default "{com.mycompany.constraints.bookrequired}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
    
    String value();	
}
