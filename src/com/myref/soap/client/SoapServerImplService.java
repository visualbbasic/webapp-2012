
package com.myref.soap.client;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "SoapServerImplService", targetNamespace = "http://server.soap.myref.com/", wsdlLocation = "http://localhost:8080/MyReferences/services/soap?wsdl")
public class SoapServerImplService
    extends Service
{

    private final static URL SOAPSERVERIMPLSERVICE_WSDL_LOCATION;
    private final static WebServiceException SOAPSERVERIMPLSERVICE_EXCEPTION;
    private final static QName SOAPSERVERIMPLSERVICE_QNAME = new QName("http://server.soap.myref.com/", "SoapServerImplService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8080/MyReferences/services/soap?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SOAPSERVERIMPLSERVICE_WSDL_LOCATION = url;
        SOAPSERVERIMPLSERVICE_EXCEPTION = e;
    }

    public SoapServerImplService() {
        super(__getWsdlLocation(), SOAPSERVERIMPLSERVICE_QNAME);
    }

    public SoapServerImplService(WebServiceFeature... features) {
        super(__getWsdlLocation(), SOAPSERVERIMPLSERVICE_QNAME);
    }

    public SoapServerImplService(URL wsdlLocation) {
        super(wsdlLocation, SOAPSERVERIMPLSERVICE_QNAME);
    }

    public SoapServerImplService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SOAPSERVERIMPLSERVICE_QNAME);
    }

    public SoapServerImplService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SoapServerImplService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName);
    }

    /**
     * 
     * @return
     *     returns SoapServerImpl
     */
    @WebEndpoint(name = "SoapServerImplPort")
    public SoapServerImpl getSoapServerImplPort() {
        return super.getPort(new QName("http://server.soap.myref.com/", "SoapServerImplPort"), SoapServerImpl.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns SoapServerImpl
     */
    @WebEndpoint(name = "SoapServerImplPort")
    public SoapServerImpl getSoapServerImplPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://server.soap.myref.com/", "SoapServerImplPort"), SoapServerImpl.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SOAPSERVERIMPLSERVICE_EXCEPTION!= null) {
            throw SOAPSERVERIMPLSERVICE_EXCEPTION;
        }
        return SOAPSERVERIMPLSERVICE_WSDL_LOCATION;
    }

}
