package com.myref.soap.server;

import javax.xml.bind.JAXBException;

import com.myref.mvc.model.Reference;
import com.myref.mvc.model.References;

public interface RefFactoryPlan {
	public void registerProduct(String type, String resource);
	public Reference createReference(String id)throws JAXBException ;
	public References createReferences(String id) throws JAXBException;
}
