package com.myref.soap.server;

import java.io.StringReader;
import java.util.HashMap;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.xml.sax.InputSource;

import com.myref.mvc.model.Reference;
import com.myref.mvc.model.References;

/**
 * @author Jason Shin
 *	ReferenceFactory creates an object of references or reference upon the request.
 */
public class ReferenceFactory implements RefFactoryPlan{

	private HashMap<String, String> productLine = new HashMap<String, String>();
	
	@Override
	public void registerProduct(String id, String resource) {
		productLine.put(id, resource);
	}

	/* (non-Javadoc)
	 * @see com.myref.soap.server.RefFactoryPlan#createReference(java.lang.String)
	 * Create a new reference object using resource and a product ID
	 */
	@Override
	public Reference createReference(String id) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(Reference.class);
		javax.xml.bind.Unmarshaller u = jc.createUnmarshaller();
		//create feeds object from XML string representation
		StringReader reader = new StringReader(productLine.get(id));
		InputSource input = new InputSource(reader);
		Reference reference = (Reference) u.unmarshal(input);
		return reference;
	}

	/* (non-Javadoc)
	 * @see com.myref.soap.server.RefFactoryPlan#createReferences(java.lang.String)
	 * create a new references object.
	 */
	@Override
	public References createReferences(String id) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(References.class);
		javax.xml.bind.Unmarshaller u = jc.createUnmarshaller();
		//create feeds object from XML string representation
		StringReader reader = new StringReader(productLine.get(id));
		InputSource input = new InputSource(reader);
		References references = (References) u.unmarshal(input);

		return references;
	}
	
}
