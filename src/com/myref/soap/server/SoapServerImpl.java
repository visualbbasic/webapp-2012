package com.myref.soap.server;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.jws.WebService;
import javax.xml.bind.JAXBException;

import com.myref.mvc.helper.ReferenceListHelper;
import com.myref.mvc.model.Reference;
import com.myref.mvc.model.References;

/**
 * @author Jason Shin
 *	Soap server implementation. It contains add, add all, delete, delete all, list all methods
 */
@WebService
public class SoapServerImpl implements SoapServer{

	@Override
	public String add(String xml){
		String productID = "first";
		ReferenceListHelper helper = new ReferenceListHelper();
		try {
			ReferenceFactory factory = new ReferenceFactory();
			factory.registerProduct(productID, xml);
			Reference reference = factory.createReference(productID);
			helper.add(reference);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			return "Please format your XML properly as image has shown";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return "Sorry, there's an error caused at the IOException";
		}
		return "New reference has been successfully added.";
	}

	@Override
	public String delete(int id){
		// TODO Auto-generated method stub
		ReferenceListHelper helper = new ReferenceListHelper();
		try {
			helper.remove(id);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return "The reference doesn't exist inside the XML file. Failed to delete";
		}
		return "Successfully deleted";
	}

	@Override
	public List<Reference> listAll() {
		// TODO Auto-generated method stub
		ReferenceListHelper helper = new ReferenceListHelper();
		try {
			return helper.references();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String addAll(String xml) {
		String productID = "first";
		ReferenceListHelper helper = new ReferenceListHelper();
		try {
			ReferenceFactory factory = new ReferenceFactory();
			factory.registerProduct(productID, xml);
			References references = factory.createReferences(productID);
			for(Reference ref : references.getReferences()){
				helper.add(ref);
			}
		} catch (JAXBException e) {
			return "Please format your XML properly as image has shown";
		} catch (IOException e) {
			return "Sorry, there's an error caused at the IOException";
		}
		return "New reference has been successfully added.";
	}

	
	@Override
	public String deleteAll(String list) {
		ReferenceListHelper helper = new ReferenceListHelper();
		String[] ids = list.split(",|,/s");
		for(int i = 0; i< ids.length; i++){
			int id = Integer.parseInt(ids[i].trim());
			try {
				helper.remove(id);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				return "Failed to remove element!: " + id;
			}
		}
		return "Successfully removed all!";
	}
	

}
