package com.myref.soap.server;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.myref.mvc.model.Reference;

/**
 * @author Jason Shin
 *  Interface for the SoapServerImpl. List of methods are add, add all, delete, delete all, list
 */
@WebService
public interface SoapServer {
	@WebMethod public String add(String xml) ;
	@WebMethod public String addAll(String xml) ;
	@WebMethod public String delete(int id) ;
	@WebMethod public String deleteAll(String list) ;
	@WebMethod public List<Reference> listAll() ;
}
